<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'indxController@index');

Route::get('/register', 'AuthController@bio');

Route::post('/welcome', 'AuthController@kirim');

Route::get('/dataTable', function(){
    return view('table.dataTable');
});

Route::get('/table', function(){
    return view('table.table');
});

// CRUD genre
Route::get('/genre/create','genreController@create');
Route::post('/genre', 'genreController@store');
