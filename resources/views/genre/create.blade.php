@extends('layout.master')

@section('Judul')
Halaman form
@endsection

@section('content')

<form action='/genre' method="POST">
  @csrf
  <div class="form-group">
    <label f>Nama Genre</label>
    <input type="text" name=nama class="form-control">
    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
  </div>
  @error('nama')
  <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection
