<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class genreController extends Controller
{
    public function create(){
      return view('genre.create');
    }
    public function store(Request $request){
      $request->validate([
        'nama' => 'required',
        'body' => 'required',
    ]);
    }
}
